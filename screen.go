package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type Screen struct {
	title                      string
	window                     *sdl.Window
	renderer                   *sdl.Renderer
	screen                     sdl.Rect
	running                    bool
	fpsCountTime, fpsCount     uint32
	bg, fg                     sdl.Color
	texBg, texFG               *sdl.Texture
	width, height, row, column int32
	rect                       sdl.Rect
	field                      *Field
	startTick, delay           uint32
}

func NewScreen(title string, width, height int32, window *sdl.Window, renderer *sdl.Renderer) *Screen {
	return &Screen{
		title:    title,
		screen:   sdl.Rect{0, 0, width, height},
		window:   window,
		renderer: renderer,
		bg:       sdl.Color{192, 192, 192, 0},
		fg:       sdl.Color{0, 0, 0, 255},
		width:    width,
		height:   height,
		row:      90,
		column:   90,
	}
}
func (s *Screen) setup() {
	s.field = NewField(int(s.row), int(s.column))
	// s.field.SetAlive(3, 5)
	// s.field.SetAlive(4, 5)
	// s.field.SetAlive(5, 5)
	// s.field.SetAlive(6, 5)
	// s.field.SetAlive(7, 5)
	s.field.Init()
	s.rect = sdl.Rect{10, 10, s.height - 20, s.height - 20}
	s.texBg = NewBackground(s.field, s.renderer, s.row, s.column, s.rect, s.fg, s.bg)
	s.startTick = sdl.GetTicks()
	s.delay = 500
}
func (s *Screen) Update() {
	if sdl.GetTicks()-s.startTick > s.delay {
		s.field.Turn()
		s.texBg = NewBackground(s.field, s.renderer, s.row, s.column, s.rect, s.fg, s.bg)
		s.startTick = sdl.GetTicks()
	}
	if sdl.GetTicks()-s.fpsCountTime > 999 {
		s.window.SetTitle(fmt.Sprintf("%s fps:%v", s.title, s.fpsCount))
		s.fpsCount = 0
		s.fpsCountTime = sdl.GetTicks()
	}
}
func (s *Screen) Render() {
	setColor(s.renderer, s.bg)
	s.renderer.Clear()
	s.renderer.Copy(s.texBg, nil, &s.rect)
	s.renderer.Present()
	s.fpsCount++
}
func (s *Screen) Event() {
	event := sdl.WaitEventTimeout(3)
	switch t := event.(type) {
	case *sdl.QuitEvent:
		s.quit()
	case *sdl.KeyboardEvent:
		if t.Keysym.Sym == sdl.K_ESCAPE && t.State == sdl.RELEASED {
			s.quit()
		}
	}
}
func (s *Screen) Init() {
	s.running = true
	s.setup()
}
func (s *Screen) Run() {
	s.Init()
	frameRate := uint32(1000 / 60)
	lastTime := sdl.GetTicks()
	for s.running {
		now := sdl.GetTicks()
		if now >= lastTime {
			i := 0
			for {
				s.Event()
				s.Update()
				lastTime += frameRate
				now = sdl.GetTicks()
				if lastTime > now {
					break
				}
				i++
				if i >= 3 {
					lastTime = now + frameRate
					break
				}
			}
			s.Render()
		} else {
			sdl.Delay(lastTime - now)
		}
	}
}
func (s *Screen) Destroy() {}
func (s *Screen) quit()    { s.running = false }
