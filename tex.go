package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

func NewBackground(field *Field, renderer *sdl.Renderer, row, column int32, rect sdl.Rect, fg, bg sdl.Color) (tex *sdl.Texture) {
	var (
		err                error
		x0, y0             float64
		x, y, w, h, dx, dy int32
	)
	tex, err = renderer.CreateTexture(sdl.PIXELFORMAT_ARGB8888, sdl.TEXTUREACCESS_TARGET, rect.W, rect.H)
	if err != nil {
		panic(err)
	}
	var cellWidth, cellHeight float64
	cellWidth, cellHeight = float64(rect.W)/float64(row), float64(rect.H)/float64(column)
	x0 = (float64(rect.W) - (cellWidth * float64(row))) / 2
	y0 = (float64(rect.H) - (cellHeight * float64(column))) / 2
	renderer.SetRenderTarget(tex)
	tex.SetBlendMode(sdl.BLENDMODE_BLEND)
	setColor(renderer, bg)
	renderer.Clear()
	setColor(renderer, sdl.Color{128, 128, 128, 255})
	renderer.DrawRect(&sdl.Rect{0, 0, rect.W, rect.H})
	for dy = 0; dy <= column; dy++ {
		for dx = 0; dx <= row; dx++ {
			x = int32(x0 + (float64(dx) * cellWidth))
			y = int32(y0 + (float64(dy) * cellHeight))
			w = int32(float64(rect.W) - x0)
			h = int32(float64(rect.H) - y0)
			renderer.DrawLine(x, int32(y0), x, h)
			renderer.DrawLine(int32(x0), y, w, y)
		}
	}
	setColor(renderer, fg)
	for idx, cell := range field.board {
		if cell.GetStatus() == cellAlive {
			xF, yF := field.GetPos(idx)
			w, h := float64(cellWidth)*0.5, float64(cellHeight)*0.5
			marginX, marginY := (cellWidth-w)/2, (cellHeight-h)/2
			x, y := float64(xF)*cellWidth+x0+marginX, float64(yF)*cellHeight+y0+marginY
			renderer.FillRect(&sdl.Rect{int32(x), int32(y), int32(w), int32(h)})
		}
	}
	renderer.SetRenderTarget(nil)
	fmt.Println(x0, y0, x, y, w, h, cellWidth, cellHeight, cellWidth*float64(row), rect.W, rect.H)
	return tex
}
