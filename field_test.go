package main

import (
	"fmt"
	"testing"
)

func TestField(t *testing.T) {
	t.Run("test field init", func(t *testing.T) {
		field := NewField(3, 3)
		got := len(field.board)
		want := 9
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
		gotCell := field.board[0].GetStatus()
		wantCell := cellEmpty
		if gotCell != wantCell {
			t.Errorf("got:%v,want:%v", gotCell, wantCell)
		}
	})
	t.Run("test field GetIdx", func(t *testing.T) {
		field := NewField(3, 3)
		got := field.GetIdx(2, 2)
		want := 8
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
	})
	t.Run("test field GetPos", func(t *testing.T) {
		field := NewField(3, 3)
		gotX, gotY := field.GetPos(8)
		wantX, wantY := 2, 2
		if gotX != wantX && gotY != wantY {
			t.Errorf("gotX:%v,gotY:%v,wantX:%v,wantY:%v", gotX, gotY, wantX, wantY)
		}
	})
	t.Run("test field GetEdge", func(t *testing.T) {
		field := NewField(3, 3)
		got := field.GetEdge(3, 3)
		want := true
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
		got = field.GetEdge(1, 1)
		want = false
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
	})
	t.Run("test field GetNeigbours", func(t *testing.T) {
		field := NewField(3, 3)
		field.SetAlive(1, 1)
		gotLen := len(field.GetNeigbours(1, 1))
		wantLen := 8
		if gotLen != wantLen {
			t.Errorf("got:%v want:%v", gotLen, wantLen)
		}
		field.SetEmpty(1, 1)
		field.SetAlive(0, 1)
		gotLen = len(field.GetNeigbours(0, 1))
		wantLen = 5
		if gotLen != wantLen {
			t.Errorf("got:%v want:%v", gotLen, wantLen)
		}
	})
	t.Run("test field GetAliveCount", func(t *testing.T) {
		field := NewField(3, 3)
		field.SetAlive(1, 1)
		got := field.GetAliveCount(2, 2)
		want := 1
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
	})
	t.Run("test field Turn", func(t *testing.T) {
		field := NewField(3, 3)
		fmt.Println(field)
		field.SetAlive(0, 1)
		field.SetAlive(1, 1)
		field.SetAlive(2, 1)
		fmt.Println(field)
		field.Turn()
		got := field.GetAliveCount(1, 1)
		want := 2
		if got != want {
			t.Errorf("got:%v want:%v", got, want)
		}
		fmt.Println(field)
	})
	t.Run("test field Turn", func(t *testing.T) {
		field := NewField(11, 11)
		field.SetAlive(3, 5)
		field.SetAlive(4, 5)
		field.SetAlive(5, 5)
		field.SetAlive(6, 5)
		field.SetAlive(7, 5)
		for i := 0; i < 10; i++ {
			fmt.Println(field)
			field.Turn()
		}
		fmt.Println(field)
	})
}
