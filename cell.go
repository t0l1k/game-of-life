package main

type cellState byte

const (
	cellEmpty cellState = iota
	cellAlive
	cellBorn
	cellDie
)

type Cell struct {
	cell cellState
}

func NewCell() *Cell {
	return &Cell{}
}

func (s *Cell) GetStatus() cellState { return s.cell }
func (s *Cell) SetEmpty()            { s.cell = cellEmpty }
func (s *Cell) SetAlive()            { s.cell = cellAlive }
func (s *Cell) SetBorn()             { s.cell = cellBorn }
func (s *Cell) SetDie()              { s.cell = cellEmpty }
func (s *Cell) String() string {
	var str string
	if s.GetStatus() == cellEmpty {
		str = "-"
	} else if s.GetStatus() == cellAlive {
		str = "*"
	} else if s.GetStatus() == cellBorn {
		str = "+"
	} else if s.GetStatus() == cellDie {
		str = "~"
	}
	return str
}
