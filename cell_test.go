package main

import (
	"testing"
)

func TestCell(t *testing.T) {
	t.Run("cell test", func(t *testing.T) {
		cell := NewCell()
		got := cell.GetStatus()
		want := cellState(cellEmpty)
		if got != want {
			t.Errorf("got:%v,want:%v", got, want)
		}
	})
	t.Run("cell test alive", func(t *testing.T) {
		cell := NewCell()
		cell.SetAlive()
		got := cell.GetStatus()
		want := cellState(cellAlive)
		if got != want {
			t.Errorf("got:%v,want:%v", got, want)
		}
		t.Log(cell)
	})
}
