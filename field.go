package main

import "math/rand"

type Field struct {
	board       []*Cell
	row, column int
}

func NewField(w, h int) *Field {
	var board []*Cell
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			cell := NewCell()
			board = append(board, cell)
		}
	}
	return &Field{
		board:  board,
		row:    w,
		column: h}
}

func (s *Field) Init() {
	for i := 0; i < int(float64(s.row*s.column)*0.5); i++ {
		x, y := rand.Intn(s.row), rand.Intn(s.column)
		if s.board[s.GetIdx(x, y)].GetStatus() == cellAlive {
			continue
		}
		s.board[s.GetIdx(x, y)].SetAlive()
	}
}

func (s *Field) Turn() {
	var newArr []cellState
	for idx, cell := range s.board {
		x, y := s.GetPos(idx)
		count := s.GetAliveCount(x, y)
		status := cell.GetStatus()
		if status == cellAlive && count < 2 || status == cellAlive && count > 3 {
			newArr = append(newArr, cellDie)
		} else if status == cellEmpty && count == 3 {
			newArr = append(newArr, cellBorn)
		} else if status == cellAlive {
			newArr = append(newArr, cellAlive)
		} else if status == cellEmpty {
			newArr = append(newArr, cellEmpty)
		}
	}
	for idx, cell := range newArr {
		x, y := s.GetPos(idx)
		if cell == cellAlive {
			s.SetAlive(x, y)
		} else if cell == cellBorn {
			s.SetAlive(x, y)
		} else if cell == cellDie {
			s.SetDie(x, y)
		} else if cell == cellEmpty {
			s.SetEmpty(x, y)
		}
	}
}

func (s *Field) GetEdge(x, y int) bool     { return !(x >= 0 && x < s.row && y >= 0 && y < s.column) }
func (s *Field) GetIdx(x, y int) int       { return y*s.column + x }
func (s *Field) GetPos(idx int) (int, int) { return idx % s.row, idx / s.row }
func (s *Field) GetNeigbours(x, y int) (arr []*Cell) {
	for dy := -1; dy < 2; dy++ {
		for dx := -1; dx < 2; dx++ {
			nx := dx + x
			ny := dy + y
			if s.GetEdge(nx, ny) {
				nx = x
				ny = y
			}
			if !(nx == x && ny == y) {
				arr = append(arr, s.board[s.GetIdx(nx, ny)])
			}
		}
	}
	return arr
}
func (s *Field) GetAliveCount(x, y int) (count int) {
	for _, cell := range s.GetNeigbours(x, y) {
		if cell.GetStatus() == cellAlive {
			count++
		}
	}
	return count
}

func (s *Field) SetEmpty(x, y int) { s.board[s.GetIdx(x, y)].SetEmpty() }
func (s *Field) SetAlive(x, y int) { s.board[s.GetIdx(x, y)].SetAlive() }
func (s *Field) SetBorn(x, y int)  { s.board[s.GetIdx(x, y)].SetBorn() }
func (s *Field) SetDie(x, y int)   { s.board[s.GetIdx(x, y)].SetDie() }

func (s *Field) String() (str string) {
	for y := 0; y < s.column; y++ {
		for x := 0; x < s.row; x++ {
			str += s.board[s.GetIdx(x, y)].String()
		}
		str += "\n"
	}
	return str
}
